Array named `group_posts` with fields:

* Array `activity_log_data` with fields:

  * `timestamp`: epoch time of when the event happened

  * `title`: what happened. Known content:

     * "<NAME> replied to <NAME>'s comment."

     * "<NAME> commented on <NAME>'s post."

     * "<NAME> posted in <GROUPNAME>.

  * `data`: either:

    1. single-element array with fields:

       * `comment`:

         * `timestamp`: epoch time, same as outer `timestamp`

         * `comment`: the actual comment

         * `author`: Name of the author

         * `group`: name of the group

    2. two-element array with:

       * item 1:

         * `post`: content of the post

       * item 2:

         * `update_timestamp`: epoch time
