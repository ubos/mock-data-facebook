## Overview

There are several directories that appear to have the same structure, which
is documented below.

* `archived_threads/`
* `filtered_threads/`
* `inbox/`
* `message_requests/`

There is also directory `stickers_used/`, which contains PNG files
that are sticker graphics. The names of the files are of form
`(\d+)_(\d+)_(\d+)_n_(\d+)\.png`, where:
* $1: 6-8 digits
* $2: about 16 digits
* $3: about 9-10 digits
* $4: about 15 digits

## Structure (e.g. of `archived_threads/`)

The message thread / inbox etc directories each contains several others
directories, which seem to be named after the contact with an appended random
identifier. e.g. `billsm_kfeaacaucw/`. There is no index.

### Subdirectories (e.g. of `archived_threads/billsm_kfeaacaucw/`)

Each directory contains exactly one file labeled `message_1.json`

Optional directory `files/` which contains attachments (?) to the messages.

Files are named `(\s+)_(\d+).(\s+)` e.g. "Animalexperiments_123456.docx"

Optional directory `photos/` which contains attachments (?) to the messages.

Photos are named `(\d+)_(\d+)_(\d+)_o_(\d+).jpg` with similar digit lengths
as above.

## `message_1.json` (e.g. `archived_threads/billsm_kfeaacaucw/message_1.json`

* `participants`: array of:

  * `name`: Name of participant

* `title`:

  * if there is one other participant, it contains their name

  * if there are many other partipants, it contains a summary such as
    "<NAME>, <NAME>, Facebook User and 41 others"

* `is_still_participant`: boolean

* `thread_type`: enum (?) with known values:

  * "Regular"

  * "RegularGroup"

* `thread_path`: path to the enclosing directory, from the `messages`
  directory, e.g.   "archived_threads/billsm_kfeaacaucw"

* `messages`: array of:

  * `sender_name`: name of the sender

  * `timestamp_ms`: epoch time, in milliseconds (?)

  * `type`: enumerated value, known values:

    * "Call"

    * "Generic"

    * "Payment"

    * "Share"

    * "Unsubscribe"

  * MESSAGES_UNION

### MESSAGES_UNION

There appear to be several varieties:

1. Variety 1

* `content`: content of the message, or text description of what happened, e.g.

  * "<NAME> left the group."

* `users` (optional): zero or single-element array with:

  * `name`  appears the same as `sender_name`.

2. Variety 2

* `sticker` with:

  * `uri`: references a file in `messages/stickers_used`, with
    a path name that starts at the root directory, e.g.
    `messages/stickers_used/1_2_3_n_4.png`.

3. Variety 3

* `photos` array with fields:

  * `uri`: pointer to file in sibling directory `photos/`, with
    a path name that starts at the root directory, e.g.
    `messages/inbox/billsm_kfeaacaucw/photos

  * `creation_timestamp`: epoch time

4. Variety 4

* `share` with fields:

  * `link`: URL

