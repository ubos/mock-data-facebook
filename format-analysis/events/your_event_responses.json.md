`event_responses` with fields:

* array named `events_joined`:

  * `name`: name of the event

  * `start_timestamp`: epoch time

  * `end_timestamp`: epoch time

* array named `events_declined`:
  (same fields as `events_joined`)

* array named `events_interested`:
  (same fields as `events_joined`)
