Array named `events_invited`, with fields:

* `name`: Name of the event

* `start_timestamp`: epoch time

* `end_timestamp`: epoch time
