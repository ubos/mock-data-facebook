Array named `your_places` with fields:

* `name`: name of a job title

* `address`: empty string

* `url`: a Facebook URL that redirects to a page that apparently
  represents this particular job title, e.g. "https://www.facebook.com/693940177305910"
  redirects to "https://www.facebook.com/pages/Committer-Instigator/693940177305910"

Note:

* "Your places" is a strange name for this data.
