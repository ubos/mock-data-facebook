Array named `admin_records` with fields:

* `event`: known values:

  * "Username"

  * "Checkpoint completed"

  * "Email change"

  * "Remove Profile Photo"

  * "Password Change"

  * "Security question and response changed"

  * "Birthday Change"

* `session` :

  * `created_timestamp`: epoch time

  * `ip_address`: ipv4 address

  * `user_agent`: Either:

    * browser user-agent string

    * empty string

  * `datr_cookie`: Either;

    * a partially blanked-out string, e.g. "-oXQ********************"

    * empty string

