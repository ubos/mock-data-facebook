Array `account_accesses` with fields:

* `action`: Known values:

  * "Login"

  * "Log Out"

* `timestamp`: epoch time

* `site`: Known values

  * "www.facebook.com"

* `ip_address`: IPv4 address
