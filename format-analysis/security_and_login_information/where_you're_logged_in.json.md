Array named `active_sessions` with fields:

* `created_timestamp`: epoch time

* `updated_timestamp`: epoch time

* `ip_address`: IPv4 address

* `user_agent`: browser user agent

* `datr_cookie`: partially-blanked cookie value, e.g. "-oXQ********************"

* `device`: known values:

  * "Mac"

  * "iPhone 8"

  * "iPad 5th Gen"

* `location`: city, state, country, e.g. "Santa Clara, CA, United States"

* `app`: known values:

  * "Firefox"

  * "Facebook app"

  * "Messenger"
