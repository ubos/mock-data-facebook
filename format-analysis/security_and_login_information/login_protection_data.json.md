Array named `login_protection_data` with fields:

* `name`: known formats:

  * "Estimated location inferred from IP <FLOAT>, <FLOAT>"

  * "IP Address: <IPv4>"

  * "IP Address: <IPv6>"

  * "Cookie: <PARTIALLY-BLANKED>" e.g. "Cookie: yDDj********************"

* `session`:

  * `created_timestamp`: epoch time

  * `updated_timestamp` (optional): epoch time

  * `ip_address` (optional): IPv4 or IPv6 address
