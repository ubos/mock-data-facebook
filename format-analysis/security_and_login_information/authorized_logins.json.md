Array named `recognized_devices` with fields:

* `name`: Name of the device. Sort of looks like enumerated value,
  but sloppily defined, e.g. there are entries "Ipad" and "ipad".

* `created_timestamp`: epoch time

* `updated_timestamp`: epoch time

* `ip_address`: IPv4 address

* `user_agent` (optional) : browser user agent

* `datr_cookie`: empty string
