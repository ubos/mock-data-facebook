Array named `account_activity`, ordered by `timestamp` with most recent first,
with fields:

* `action`: Known values:

  * "Session updated"

  * "Login"

  * "Web Session Terminated"

  * "Payments in Messenger transaction"

  * "Attempted Adding CC"

* `timestamp`: epoch time

* `ip_address`: ipv4 or ipv6 address

* `user_agent`: Looks like a browser user-agent string

* `datr_cookie` (optional): a partially blanked-out string, e.g.
   "-oXQ********************"
