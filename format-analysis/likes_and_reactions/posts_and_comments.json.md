Array named `reactions` with fields:

* `timestamp`: epoch time

* `title`: what happened, with known values:

  * "<NAME> likes <NAME>'s post."

* `data`: single-element array of:

  * `reaction`:

    * `reaction`: Enum value (?) with known values:

      * "LIKE"

    * `actor`: Name of the person performing the action

NOTE:

* There is no information about what is actually being liked.


