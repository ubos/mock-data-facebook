Array named `other_likes` with fields:

* `timestamp`: epoch time

* `title`: text description of what happened. Known values:

  * "<NAME> likes <PAGETITLE>."

* `attachments`: single-element array of:

  * `data`: single-element array of:

    * `external_context`:

      * `url`: the URL of the liked page
