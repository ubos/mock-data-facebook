* `payments`:

  * `preferred_currency`: 3-letter currency code, e.g. "USD"

  * `payments`: array with fields:

    * `created_timestamp`: epoch time

    * `amount`: amount transferred, with decimal point and no unit, e.g. "100.00"

    * `currency`: 3-letter currency code

    * `sender`: name of the sender

    * `receiver`: name of the receiver

    * `type`: enumerated value with known values:

      * "Payment"

    * `status`: enumerated value with known values:

      * "Completed"

    * `payment_data`:

      * `p2p_payment` :

        * `total_amount`: amount with currency symbol, e.g. "$100.00". Unclear
          how this relates to `amount` and `currency` above

    * `payment_method`: Unclear what values are allowed here. One example:

      * "Mastercard ending in 1234"
