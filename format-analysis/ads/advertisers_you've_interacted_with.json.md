Array `history` with entries:

* `title`: the call to action of the ad?

* `action`: enumerated value (?), known values:

  * `Clicked Ad`

* `timestamp`: epoch time
