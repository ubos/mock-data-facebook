## Description

This seems to contain a number of fixed-named directories (see below)
and a number of directories named with a understandable prefix, followed by _ and
a random character string.

## `album/`

Contains N JSON files named:

* `0.json`
* `1.json`

etc. each of which is a separate album.

## `thumbnails/`

Contains JPG files, named `(\d+)_(\d+)_(\d+)_n.jpg`

## `videos/`

Contains MP4 files, named `(\d+)_(\d+)_(\d+)_n_(\d+).mp4`

## `your_posts/`

Contains JPG files `(\d+)_(\d+)_(\d+)_[no]_(\d+).jpg`

