Array named `videos` with fields:

* `title`: empty string

* `description`: text

* `creation_timestamp` : epoch time

* `uri` : a filename, pointing into a sibling directory from the
  root of the zip file, e.g. starting with "photos_and_videos/videos/"

* `media_metadata` :

  * `video_metadata` :

    * `upload_ip` : IPv4

    * `upload_timestamp` : epoch time, may be 0

* `thumbnail` :

  * `uri`: a filename, pointing into a sibling directory from the
  root of the zip file

* `comments` : array of:

  * `timestamp`: epoch time

  * `comment`: text of the comments

  * `author`: author of the comment
