Array named `pages_unfollowed` with fields:

* `title`: Title of the page

* `timestamp`: epoch time

* `data`: single-element array with

  * `name`: Same as `title`

Note:

* unclear whether `timestamp` refers to the time the following started,
  or the following ended.

