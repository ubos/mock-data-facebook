Array name `pages_followed` with fields:

* `title`: Title of the page

* `timestamp`: epoch time

* `data`: single-element array with

  * `name`: Same as `title`
