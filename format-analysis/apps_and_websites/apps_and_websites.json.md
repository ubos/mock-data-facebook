Array of `installed_apps`, with fields:

* `name`: name of the app, e.g. "Quora"

* `added_timestamp`: epoch time
