Array named `admined_apps`, with fields:

* `name` : domain name of a website

* `added_timestamp`: always 0

Notes:

* Websites listed here must also be listed in `apps_and_websites.json`.

* The value for `added_timestamp` here is incorrect and redundant,
  as there is a correct value in `apps_and_websites.json`.
