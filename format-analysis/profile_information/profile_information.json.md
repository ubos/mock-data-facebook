* `profile` with:

  * `name` :

    * `full_name`

    * `first_name`

    * `middle_name`: may be empty string (not null)

    * `last_name`

  * `emails`:

    * `emails` : array of e-mail addresses

    * `previous_emails` : array of e-mail addresses

    * `pending_emails`: array of e-mail addresses

    * `ad_account_emails`: array of e-mail addresses (what is this?)

  * `birthday`:

    * `year`: integer

    * `month`: integer

    * `day`: integer

  * `gender`:
    * `gender_option`: enumerated domain, known values:

      * "MALE"

    * `pronoun`: enumerated domain, known values:

      * "MALE"

  * `previous_names`: array of ?

  * `other_names`: array of ?

  * `current_city`:

    * `name`: city comma state

    * `timestamp`: is 0

  * `relationship`:

    * `status`: enumerated domain, known values:

      * "Married"

  * `family_members`: array of

    * `name`: full name

    * `relation` : enumerated domain, known values:

      * "Sister-in-law"

  * `education_experiences` : array of

    * `name`: name of the educational institution

    * `end_timestamp` (optional): epoch time

    * `graduated`: boolean

    * `concentrations`: array of String, e.g. `Electrical engineering`

    * `degree` : String e.g. `Doctorate`

    * `school_type`: String e.g. `College`, `Graduate School`

  * `work_experiences` : array of

    * `employer`: name of business

    * `title`: job title

    * `location`: city comma state

    * `description`: text

    * `start_timestamp` : epoch time

  * `languages` : array of string, e.g. `English language`, `German language`.

  * `political_view` :

    * `name`: string

    * `description`: text

  * `religious_view` :

    * `name`: string

    * `description`: text

  * `websites`: array of

    * `address`: URL

  * `phone_numbers`: array of

    * `phone_type`: enumerated domain, with known values:

      * "Mobile"

    * `phone_number`: phone number as string, with leading + country code

    * `verified`: boolean

  * `username`: string without blanks

  * `about_me`: text

  * `pages` : array of

    * `name`: some kind of category name? E.g. `Books`, `Movies`, `Other`.
      The "Other" category seems to have the same content as `likes_and_reactions/page_likes.json`
      except that the movies have been taken out and put into "Movies".

    * `pages`: array of String

  * `groups`: array of

    * `name`: name of the group

    * `timestamp`: epoch time

  * `registration_timestamp`: epoch time

  * `profile_uri`: URL on facebook
