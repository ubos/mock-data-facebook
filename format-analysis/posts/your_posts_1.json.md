Array of: (top-level element indeed is an array)

* `timestamp`: epoch time

* There seem to be several forms:

  1. Form 1: title

     * `title`: author or action. Known formats:

       * "<NAME>"

       * "<NAME> updated his status."

       * "<NAME> added a new photo to <NAME>'s timeline."

       * "<NAME> added a new photo."

       * "<NAME> commented on a link."

       * "<NAME> followed a person on Goodreads."

       * "<NAME> likes a link."

       * "<NAME> likes an article."

       * "<NAME> likes an event on Eventbrite."

       * "<NAME> listened to <TITLE> on Spotify."

       * "<NAME> posted in <GROUPNAME>."

       * "<NAME> recently crossed paths with a person on Highlight."

       * "<NAME> recommends a link."

       * "<NAME> shared a memory."

       * "<NAME> shared an episode of <TITLE>."

       * "<NAME> subscribed to a playlist on Spotify."

      * "<NAME> updated his status."

       * "<NAME> wants to read a book on Goodreads."

       * "<NAME> was live."

       * "<NAME> wrote on <NAME>'s timeline."

       * "<NAME> wrote on a timeline."

     * `data`: seems to be a heterogeneous array with 1 or 2 elements:

       * `post`: the content of the post

       * `update_timestamp` (optional): epoch time

     * `attachments`: single-element array of

       * `data`: single-element array of

         * `external_context`:

           * `url`: URL

  2. Form 2: media object instead of title

     * `attachments`: single-element array of

       * `data`: single-element array of

         * `media`:

           * `uri`: points to file in `photos_and_videos/...`

           * `creation_timestamp`: epoch time

           * `media_metadata` :

             * `latitude`: float

             * `longitude`: float

             * `orientation`: `1` (are there other values, maybe `0`?)

             * `upload_ip`: IP address

           * `title`: title such as `Mobile Uploads`

           * `description`: same as `data`/`post`

  3. Form 3:

     * `attachments`: single-element array of

       * `data`: single-element array of

         * `place`:

           * `name`: Name of the place, e.g. "Pizza & Pints"

           * `coordinate`:

             * `latitude`: float

             * `longitude`: float

           * `address`: Street address, city and zip in one line

           * `url`: URL to Facebook page
