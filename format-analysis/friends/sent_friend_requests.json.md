Array named `sent_requests` with fields:

* `name`: name of the friend

* `timestamp`: epoch time when request was made
