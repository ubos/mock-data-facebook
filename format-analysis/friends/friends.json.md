Array named `friends.json` with fields:

* `name`: name of the friend

* `timestamp`: epoch time (when connection was made?)

* `contact_info` (optional): seems to be an e-mail address

