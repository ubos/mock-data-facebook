Array named `rejected_friend_requests.json` with fields:

* `name`: name of the rejected friend:

* `timestamp`: epoch time when friend was rejected (?)

* `marked_as_spam` (optional): `true`
