Array named `poll_votes` with fields:

* `timestamp`: epoch time

* `title`: what happened. Known formats:

  * "<NAME> voted on <NAME>'s poll."

* `attachments`: single-element array with:

  * `data`: single-element array with:

    * `poll`:

      * `options`: single-element array with:

        * `option`: name of the option

        * `voted`: "true"
