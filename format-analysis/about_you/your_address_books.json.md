Array named `address_book`, with fields:

* `name` eg "Jim Smith"

* `created_timestamp` as epoch time e.g. `1504901568`

* `updated_timestamp` as epoch time e.g. `1504901568`

* `details`: an array, mostly empty.

  * may contain a `contact_point` with value `middle_name: Elizabeth`
    (name-value as string)
