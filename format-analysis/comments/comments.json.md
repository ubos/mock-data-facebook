Array named `comments` with:

* `timestamp`: epoch time

* `title`: English prose describing what happened, known domain:

  * "<Name> replied to <Name>'s comment."

  * "<Name> commented on <Name>'s post."

  * "<Name> commented on his own post."

  * "<Name> replied to his own comment."

* `data`: array of 1 data element:

  * `comment`

    * `timestamp`: epoch time. Appears to be the same as the outer one,
      except (1) may (rarely) be 1 second later, (2) may be 0 in case
      of comments on external sites

    * `comment`: the actual comment text

    * `author`: name of the author of the comment

    * `group` (optional): Name of the Facebook group where the comment was made

* `attachments` (optional): array of 1 data element:

  * `data`: array of ` data element

    * `external_context`:

      * `url`: URL of the post being commented on

Notes:

* does not contain any context: which thread / post this was a comment for.
