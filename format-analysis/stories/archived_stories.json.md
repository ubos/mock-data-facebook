Array named `archived_stories` with fields:

* `timestamp`: epoch time

* `title`: what happened. Known formats:

  * "A photo from <NAME>'s story was added to his archive."

  * "<NAME>'s photo from <NAME>'s birthday story was added to <FIRSTNAME>'s archive."

* `attachments`: single-element array with:

  * `data`: single-element array with:

    * `title`: empty string

    * `media`:

      * `creation_timestamp`: epoch time

      * `uri`: relative URL to jpg, starting with `photos_and_videos/`

      * `media_metadata`:

        * `photo_metadata` :

          * `upload_ip`: IPv4 address
