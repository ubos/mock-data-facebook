Array named `searches` with fields:

* `timestamp`: epoch time

* `title`: name of the activity, with known values:

  * "You searched Facebook"

* `data`: single-element array with fields:

  * `text`: search term

* `attachments`: single-element array with fields:

  * `data`: single-element array with fields:

    * `text`: search term, same as `text` above, but within double quotes
