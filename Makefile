# Create the mock export data zip

default : generated/facebook-williamsmith-20200106.zip

# normal build
generated/facebook-williamsmith-20200106.zip : .FORCE
	[[ -d generated ]] || mkdir generated
	[[ -e $@ ]] && rm $@ || true
	cd william-smith/data-20200106 && zip -r ../../$@ *

# release build
release :
ifndef TAG
	$(error Must provide a TAG var, e.g. "TAG=v0.0.1 make release")
endif
	git tag -a "$(TAG)"
	[[ -d releases ]] || mkdir releases
	cd william-smith/data-20200106 && zip -r ../../releases/facebook-williamsmith-20200106-${TAG}.zip *

upload :
	[[ -d uploads ]] || mkdir uploads
	cp releases/* uploads/
	cp `ls -1t releases/facebook-williamsmith-20200106-*.zip | head -1` uploads/facebook-williamsmith.zip
	cd uploads && python ../bin/generate-index.py > index.html
	aws s3 sync --delete --acl public-read uploads s3://depot.ubos.net/mockdata/facebook

clean :
	rm generated/* uploads/*

.PHONY: default release upload clean

.FORCE:

